/*global jQuery */
//код из formcarry
 $(function(){
    $(".ajaxForm").submit(function(e){
           e.preventDefault();
        var href = $(this).attr("action");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: href,
            data: $(this).serialize(),
            success: function(response){
                if(response.status == "success"){
                    alert("Форма отправлена, спасибо!");
                    //очистка формы после корректной отправки
                    localStorage.removeItem("clientname");
                    localStorage.removeItem("box");
                    localStorage.removeItem("text");
                    localStorage.removeItem("check");
                    clientname.value = localStorage.getItem("clientname");
                    box.value = localStorage.getItem("box");
                    text.value = localStorage.getItem("text");
                    check1.checked = false;
                } else{
                    alert("Вот такая ошибка: " + response.message);
                }
            }
        });
    });
});


window.addEventListener("DOMContentLoaded", function () {
    var clientname = document.getElementById("clientname");
    clientname.oninput = save;
    var box = document.getElementById("box");
    box.oninput = save;
    var text = document.getElementById("text");
    text.oninput = save;
    var check1 = document.getElementById("check1");
    check1.onchange = save;
    //поиск всех элементов формы, запоминание на всю жизнь страницы
    //и присваивание обработчика изменений

    clientname.value = localStorage.getItem("clientname");
    box.value = localStorage.getItem("box");
    text.value = localStorage.getItem("text");
    var q = localStorage.getItem("check1");
    if (q==1) {
        check1.checked = true;
    }
    if (q==0){
        check1.checked = false;
    }
    //восстановление значений

    var popup = document.querySelector(".popup__overlay");
    var btn = document.getElementById("button1");
    var close = document.querySelector(".close");

    btn.addEventListener("click", function(event){
        event.preventDefault();
        popup.classList.remove("hidden");
        window.history.pushState({page: 1}, "Main", "/web8");
        window.history.pushState({page: 2}, "Form", "/web8/#form");
        //history.replaceState({page: 3}, "title 3", "?page=3");
        //window.history.pushState({page: 'form'}, '', '/form');
    });
    //вызов окна по кнопке

    popup.addEventListener("click", function(event) {
      e = event || window.event;
      if (e.target == this) {
        popup.classList.add("hidden");
        window.history.back();
      }
    });
    //нажатие за пределы формы

    close.addEventListener("click", function(event){
        event.preventDefault();
        popup.classList.add("hidden");
        window.history.back();
    });
    //по нажатии на кнопку

    window.addEventListener("popstate", function(event) {
        if ((document.location.toString().indexOf("form")+1)==0) {
            popup.classList.add("hidden");
        }
        else {
            popup.classList.remove("hidden");
        }
        //console.log(document.location.toString().indexOf("form")+1);
});
//корректная обработка кнопок движение по истории в браузере
});

function save(){
  //let text = document.getElementById("text");
  localStorage.setItem("clientname", clientname.value);
  //console.log(clientname.value);
  localStorage.setItem("box", box.value);
  localStorage.setItem("text", text.value);
  if (check1.checked) {
      localStorage.setItem("check1",1);
  }
  else {
      localStorage.setItem("check1",0);
  }
  //сохранение при любом изменении полей

}